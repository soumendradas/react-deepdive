import { useState } from "react";


export default function Player({intialName, symbol, isActive}){

    const [playerName, setPlayerName] = useState(intialName);

    const[ isEditing, setIsEditing] = useState(false);

    function handleButtonClick(){
        // setIsEditing(isEditing?false:true);
        // ! mark is the inverted the value
        // setIsEditing(!isEditing); // => schedules a state update to true 
        // setIsEditing(!isEditing); // => schedules a state update to true


        //react recomandation for update the state
        setIsEditing((editing)=> !editing);     // => schedules a state update to true 
        // setIsEditing((editing)=> !editing);     // => schedules a state update to true 

    }
    
    //event parameter taken user input value
    function handleChange(event){
        console.log(event);
        setPlayerName(event.target.value);
    }

    let editablePlayerName = <span className="player-name">{playerName}</span>;
    let btnCaption = "Edit";
    if(isEditing){
        editablePlayerName = <input type="text" required value={playerName} onChange={handleChange}/>
        btnCaption="Save";
    }

    return(
        <li className={isActive?'active':undefined}>
            <span className="player">
                {editablePlayerName}
              <span className="player-symbol">{symbol}</span>
            </span>
            <button onClick={handleButtonClick}>{btnCaption}</button>
            {/* <button onClick={handleButtonClick}>{isEditing? "Save":"Edit"}</button> */}
          </li>
    );
}
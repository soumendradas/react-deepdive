import { useState } from "react";

const initialGameBoard = [
    [null, null, null],
    [null, null, null],
    [null, null, null]
];

export default function GameBoard({onSelectSquare, turns}){
    // const [gameBoard, setGameBoard] = useState(initialGameBoard);
    // function handleSelectedSquare(rowIndex, colIndex){
    //     setGameBoard((prevGameBoard)=>{
    //         const updateGameBoard = [...prevGameBoard.map(innerArray=> [...innerArray])];    // its important to copy the array when we change the value
    //         updateGameBoard[rowIndex] [colIndex] = activePlayerSymbol;
    //         return updateGameBoard;
    //     });
    //     onSelectSquare();
    // }

    let gameBoard = initialGameBoard;

    for(const turn of turns){
        const{square, player} =turn;   //object destructuring

        const{row, col} = square;
        gameBoard[row][col] = player;
    }

    return(
        <ol id="game-board">
            {gameBoard.map((row, rowIndex)=>(<li key={rowIndex}>
                <ol>
                    {row.map((playerSymbol, colIndex)=>
                    <li key={colIndex}>
                        <button onClick={()=>onSelectSquare(rowIndex, colIndex)}
                                disabled={playerSymbol !==null}>{playerSymbol}</button></li>)}
                </ol>
                </li>))}
        </ol>
    );
}